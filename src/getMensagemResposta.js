import uid from 'uuid/v4'

import { gerarDocumentoInstanciado } from './gerarDocumentoInstanciado'
import { gerarDadosPreview } from './gerarDadosPreview'
import { enumStatusFila } from './utils'

export const enumOperacoes = {
  geracaoDePreview: 'geracaoDePreview',
  instanciamento: 'instanciamento',
}

const geracaoDadosParaResposta = {
  [enumOperacoes.geracaoDePreview]: gerarDadosPreview,
  [enumOperacoes.instanciamento]: gerarDocumentoInstanciado,
}

const getOpcoes = operacao => {
  let opcoes = { embaralharAlternativas: true }
  if (operacao.informacoes) {
    opcoes = { ...opcoes, ...operacao.informacoes.opcoesInstanciamento }
    delete operacao.informacoes.opcoesInstanciamento
  }
  return { opcoes, operacao }
}

const gerarDadosResposta = ({ dados, operacao }) => {
  let opcoes
  const gerar = geracaoDadosParaResposta[operacao.tipo]
  ;({ opcoes, operacao } = getOpcoes(operacao))
  if (gerar) dados = gerar({ dados, opcoes })
  else
    throw new Error(
      `Função de geração de dados para resposta não encontrada para o tipo de operação informado.
      Tipo de operação: ${operacao.tipo}`
    )
  return { dados, operacao }
}

export const getMensagemResposta = mensagemRecebida => {
  let { dados, operacao } = mensagemRecebida
  const statusInstanciamento = enumStatusFila.pronto
  ;({ dados, operacao } = gerarDadosResposta({ dados, operacao }))
  return { id: uid(), operacao, statusInstanciamento, dados }
}
