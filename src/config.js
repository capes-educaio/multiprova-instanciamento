export default {
  rabbit: {
    server_url: process.env.RABBIT_SERVER_URL || 'amqp://localhost',
    server_test_url: process.env.RABBIT_SERVER_TESTE_URL || 'amqp://localhost',
  },
  web: {
    protocolo: process.env.WEB_PROTOCOLO || 'http',
    port: process.env.WEB_PORT || 3100,
    hostname: process.env.API_HOSTNAME || 'localhost',
  },
  tempo_expiracao_pos_download_minutos:
    process.env.TEMPO_EXPIRACAO_POS_DOWNLOAD_MINUTOS | 10,
  tempo_expiracao_minutos: process.env.TEMPO_EXPIRACAO_MINUTOS | 20,
  monitoramento_expiracao_minutos:
    process.env.MONITORAMENTO_EXPIRACAO_MINUTOS | 2,
}
