export const enumStatusFila = {
  fila: 'EM FILA',
  processandoJson: 'PROCESSANDO JSON',
  gerandoInstancias: 'GERANDO INSTANCIAS',
  pronto: 'PRONTO',
  error: 'ERROR',
}
