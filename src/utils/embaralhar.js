const getNovoIndex = index => Math.floor(Math.random() * (index + 1))

const trocarPosicoes = (array, posicao1, posicao2) => {
  ;[array[posicao1], array[posicao2]] = [array[posicao2], array[posicao1]]
}

export const embaralhar = arrayOriginal => {
  const array = [...arrayOriginal]
  for (let index = array.length - 1; index > 0; index--) {
    const novoIndex = getNovoIndex(index)
    trocarPosicoes(array, index, novoIndex)
  }
  return array
}
export const embaralharComRestricao = arrayOriginal => {
  const array = [...arrayOriginal]
  for (let index = array.length - 1; index > 0; index--) {
    if (!array[index].fixo && !array[index].fixa) {
      let novoIndex = getNovoIndex(index)
      while (array[novoIndex].fixo || array[novoIndex].fixa) novoIndex = getNovoIndex(index)
      trocarPosicoes(array, index, novoIndex)
    }
  }
  return array
}
