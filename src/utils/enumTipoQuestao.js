export const enumTipoQuestao = {
  multiplaEscolha: 'multiplaEscolha',
  vouf: 'vouf',
  discursiva: 'discursiva',
  associacaoDeColunas: 'associacaoDeColunas',
  bloco: 'bloco',
  redacao: 'redacao',
}
