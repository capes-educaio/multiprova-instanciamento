export const enumTipoTemplate = {
  multiplaEscolhaGeral: 'geral/previewMultiplaEscolha',
  blocoGeral: 'geral/previewBloco',
  provaGeral: 'geral/prova',
  cadernoGeral: 'geral/caderno',

  multiplaEscolhaComperve: 'comperve/previewMultiplaEscolha',
  blocoComperve: 'comperve/previewBloco',
  provaComperve: 'comperve/prova',
  cadernoComperve: 'comperve/caderno',
}
