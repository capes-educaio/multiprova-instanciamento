import { enumTipoQuestao } from './enumTipoQuestao'

export const enumTiposDadosInstanciamento = {
  concurso: 'concurso',
  caderno: 'caderno',
  prova: 'prova',
  ...enumTipoQuestao,
}
