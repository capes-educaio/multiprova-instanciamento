import { gerarDocumentoInstanciadoProva } from './models/prova'
import { gerarDocumentoInstanciadoCadernoConcurso } from './models/concurso'

const funcoesGerarDocumentoInstanciado = {
  cadernoConcurso: gerarDocumentoInstanciadoCadernoConcurso,
  prova: gerarDocumentoInstanciadoProva,
}

export const gerarDocumentoInstanciado = ({ dados, opcoes }) => {
  const gerar = funcoesGerarDocumentoInstanciado[dados.tipo]
  if (gerar) {
    return gerar({ dados, opcoes })
  } else {
    throw new Error(`Função de gerar documento instanciado não achada para tipo: ${dados.tipo}`)
  }
}
