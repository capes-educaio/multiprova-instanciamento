import { instanciarProva } from './models/prova'
import { funcoesInstanciarQuestao } from './models/questao'
import { instanciarCaderno } from './models/caderno'
import { instanciarConcurso } from './models/concurso'

const funcoesInstanciar = {
  concurso: instanciarConcurso,
  caderno: instanciarCaderno,
  prova: instanciarProva,
  ...funcoesInstanciarQuestao,
}

export const gerarDadosPreview = ({ dados, opcoes }) => {
  const instanciar = funcoesInstanciar[dados.tipo]
  if (instanciar) {
    return instanciar(dados, opcoes)
  } else {
    throw new Error(`Função de instanciar não achada para tipo: ${dados.tipo}`)
  }
}
