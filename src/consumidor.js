import amqp from 'amqplib/callback_api'
import config from 'config'
import uid from 'uuid/v4'

import { sendToQueue } from './sendToQueue'
import { getMensagemResposta } from './getMensagemResposta'
import { enumStatusFila } from './utils'

const QUEUE_REQUEST_INSTANCIAMENTO = 'request-created-instances'
const QUEUE_RESPONSE_INSTANCIAMENTO = 'response-created-instances'

const URL_RABBIT = config.rabbit.server_url

export class Consumidor {
  conexao = null
  canal = null
  idFila = null
  processando = {}

  iniciarConsumidor(sucess, error) {
    console.log('Conectando ao servidor do RabbitMQ no seguinte endereço: %s', URL_RABBIT)
    console.log('Inicilizando o um consumidor para a fila %s.', QUEUE_REQUEST_INSTANCIAMENTO)
    amqp.connect(URL_RABBIT, (err, conexao) => {
      if (err) {
        console.error(
          'Falha ao tentar se conectar com o broker na seguinte URL ' +
            URL_RABBIT +
            'O serviço do Rabbit está rodando? Cheque o serviço se a URL e porta estão corretos.'
        )
        error(err)
      } else {
        this.conexao = conexao
        console.log('Conectado ao servidor do Rabbit para a fila %s. Irá criar o canal.', QUEUE_REQUEST_INSTANCIAMENTO)
        this.criarCanal(conexao, sucess, error)
      }
    })
  }

  shutdownConsumidor() {
    console.log(`Desligando o consumidor.`)
    if (this.conexao) this.conexao.close()
    else console.error('Tentando desligar conexao não existente')
  }

  criarCanal(conexao, success, error) {
    conexao.createChannel((erro, canal) => {
      if (erro) {
        error(erro)
      } else {
        this.canal = canal
        console.log('Canal de comunicação criado para a fila %s.', QUEUE_REQUEST_INSTANCIAMENTO)
        canal.assertQueue(QUEUE_REQUEST_INSTANCIAMENTO, { durable: true })
        canal.prefetch(1)
        console.log('Aguardando mensagens para a fila %s.', QUEUE_REQUEST_INSTANCIAMENTO)
        this.registrarConsumidor(this.canal)
        success()
      }
    })
  }

  registrarConsumidor() {
    this.canal.consume(QUEUE_REQUEST_INSTANCIAMENTO, this.consumir, {
      noAck: false,
    })
  }

  enviarRespostaComErro = instanciaFila => {
    try {
      const mensagemRecebida = JSON.parse(instanciaFila.content)
      const { operacao } = mensagemRecebida
      const mensagemResposta = { id: uid(), statusInstanciamento: enumStatusFila.error, operacao }
      this.enviarResposta(mensagemResposta)
    } catch (error) {
      console.error(`Falha ao eviar resposta com erro.`, error)
    }
  }

  enviarResposta = mensagemResposta => {
    this.canal.assertQueue(QUEUE_RESPONSE_INSTANCIAMENTO, { durable: true })
    sendToQueue({ ch: this.canal, queue: QUEUE_RESPONSE_INSTANCIAMENTO, mensagem: mensagemResposta })
  }

  consumir = instanciaFila => {
    this.canal.ack(instanciaFila)
    try {
      const mensagemRecebida = JSON.parse(instanciaFila.content)
      const mensagemResposta = getMensagemResposta(mensagemRecebida)
      this.enviarResposta(mensagemResposta)
    } catch (error) {
      console.error(`Erro ao consumir mensagem`, error)
      this.enviarRespostaComErro(instanciaFila)
    }
  }
}
