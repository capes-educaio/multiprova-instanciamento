export const instanciarQuestaoAssociacaoDeColunas = (questao, { embaralharAlternativas }) => {
  if (!questao) throw new Error('O objeto questao é inválido')
  const { associacaoDeColunas } = questao
  let { colunaA, colunaB } = associacaoDeColunas
  let colunaAInstanciada = instanciarColunaA(colunaA)
  let colunaBInstanciada = instanciarColunaB(colunaB)

  if (embaralharAlternativas) {
    colunaAInstanciada = embaralharColuna(colunaAInstanciada, trocarConteudoItens)
    colunaBInstanciada = embaralharColuna(colunaBInstanciada, trocarItens)
  }
  return {
    questaoMatrizId: questao.id,
    numeroNaMatriz: questao.numeroNaMatriz,
    numeroNaInstancia: questao.numeroNaInstancia,
    dataCadastro: questao.dataCadastro,
    dataUltimaAlteracao: questao.dataUltimaAlteracao,
    enunciado: questao.enunciado,
    dificuldade: questao.dificuldade,
    anoEscolar: questao.anoEscolar,
    tipo: questao.tipo,
    status: questao.status,
    elaborador: questao.elaborador,
    fixa: questao.fixa,
    valor: questao.valor,
    associacaoDeColunas: {
      colunaA: colunaAInstanciada,
      colunaB: colunaBInstanciada,
    },
  }
}
const instanciarColunaA = coluna => {
  return coluna.map((item, index) => ({
    conteudo: item.conteudo,
    rotulo: item.rotulo,
    rotuloMatriz: item.rotulo,
    posicaoMatriz: index,
  }))
}
const instanciarColunaB = coluna => {
  return coluna.map((item, index) => ({
    conteudo: item.conteudo,
    rotuloMatriz: item.rotulo,
    posicaoMatriz: index,
    respostaCandidato: null,
    dataRespostaCandidato: null,
  }))
}
const trocarConteudoItens = (coluna, origem, destino) => {
  const valor = {
    ...coluna[origem],
    rotulo: coluna[destino].rotulo,
  }
  coluna[origem] = {
    ...coluna[destino],
    rotulo: coluna[origem].rotulo,
  }
  coluna[destino] = valor
  return coluna
}
const trocarItens = (coluna, origem, destino) => {
  const valor = coluna[origem]
  coluna[origem] = coluna[destino]
  coluna[destino] = valor
  return coluna
}
const embaralharColuna = (coluna, processarItens) => {
  let origem = coluna.length,
    destino
  while (origem) {
    destino = Math.floor(Math.random() * origem)
    origem--
    coluna = processarItens(coluna, origem, destino)
  }
  return coluna
}

if (process.env.NODE_ENV === 'test') {
  exports.embaralharColuna = embaralharColuna
  exports.instanciarColunaA = instanciarColunaA
  exports.instanciarColunaB = instanciarColunaB
  exports.trocarConteudoItens = trocarConteudoItens
  exports.trocarItens = trocarItens
}
