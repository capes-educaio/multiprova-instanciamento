import { instanciarQuestoes } from './instanciarQuestao'

export const instanciarQuestaoBloco = (questao, opcoes) => {
  return {
    questaoMatrizId: questao.id,
    enunciado: questao.enunciado,
    tipo: questao.tipo,
    bloco: {
      fraseInicial: questao.bloco.fraseInicial,
      questoes: instanciarQuestoes(questao.bloco.questoes, opcoes),
    },
    fixa: questao.fixa,
  }
}
