export const instanciarQuestaoVouF = (questao, { embaralharAlternativas }) => {
  if (!questao) throw new Error('O objeto questao é inválido')
  const { vouf } = questao
  const afirmacoesEmbaralhadas = embaralharAlternativas
    ? instanciarEembaralharAfirmacoes(vouf.afirmacoes)
    : instanciarAfirmacoes(vouf.afirmacoes)
  return {
    questaoMatrizId: questao.id,
    numeroNaMatriz: questao.numeroNaMatriz,
    numeroNaInstancia: questao.numeroNaInstancia,
    fixa: questao.fixa,
    valor: questao.valor,
    enunciado: questao.enunciado,
    dificuldade: questao.dificuldade,
    tipo: questao.tipo,
    vouf: {
      afirmacoes: afirmacoesEmbaralhadas,
    },
  }
}

const instanciarEembaralharAfirmacoes = afirmacoes => {
  if (!Array.isArray(afirmacoes)) throw new Error('O objeto afirmacoes não é array')
  const afirmacoesInstanciadas = instanciarAfirmacoes(afirmacoes)
  return embaralharAfirmacoes(afirmacoesInstanciadas)
}

const instanciarAfirmacoes = afirmacoes =>
  afirmacoes.map(({ texto, letra }, posicaoNaMatriz) => ({
    texto,
    posicaoNaMatriz,
    letra,
    respostaCandidato: null,
    dataRespostaCandidato: null,
  }))

const embaralharAfirmacoes = afirmacoesOrigem => {
  let afirmacoes = [...afirmacoesOrigem]
  let indice = afirmacoes.length,
    valor,
    destino
  while (indice) {
    destino = Math.floor(Math.random() * indice)
    indice--

    valor = afirmacoes[indice]

    afirmacoes[indice] = afirmacoes[destino]
    afirmacoes[destino] = valor
  }
  return afirmacoes
}

if (process.env.NODE_ENV === 'test') {
  exports.instanciarEembaralharAfirmacoes = instanciarEembaralharAfirmacoes
  exports.instanciarAfirmacoes = instanciarAfirmacoes
  exports.embaralharAfirmacoes = embaralharAfirmacoes
}
