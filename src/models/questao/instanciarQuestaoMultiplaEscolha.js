import { embaralhar } from 'utils/embaralhar'

const ALFABETO = Object.freeze('abcdefghijklmnopqrstuvwxyz'.split(''))

const instanciarAlternativas = alternativas => {
  return alternativas.map(({ texto, letra }, index) => ({
    texto,
    letraNaInstancia: ALFABETO[index],
    letraNaMatriz: letra,
  }))
}

const acharNovaRespotaCerta = (alternativas, respostaCerta) => {
  const posicaoNoAlfabeto = alternativas.findIndex(({ letraNaMatriz }) => respostaCerta === letraNaMatriz)
  return ALFABETO[posicaoNoAlfabeto]
}

export const instanciarQuestaoMultiplaEscolha = (questao, { embaralharAlternativas }) => {
  if (!questao) throw new Error('O objeto questao é inválido')
  const { multiplaEscolha } = questao
  let { alternativas, respostaCerta } = multiplaEscolha
  if (embaralharAlternativas) alternativas = embaralhar(alternativas)
  alternativas = instanciarAlternativas(alternativas)
  return {
    questaoMatrizId: questao.id,
    fixa: questao.fixa,
    valor: questao.valor,
    enunciado: questao.enunciado,
    dificuldade: questao.dificuldade,
    tipo: questao.tipo,
    multiplaEscolha: {
      respostaCerta: acharNovaRespotaCerta(alternativas, respostaCerta),
      alternativas,
      alternativasPorLinha: questao.multiplaEscolha.alternativasPorLinha,
      respostaCandidato: null,
      dataRespostaCandidato: null,
    },
  }
}
