import { instanciarQuestaoMultiplaEscolha } from './instanciarQuestaoMultiplaEscolha'
import { instanciarQuestaoBloco } from './instanciarQuestaoBloco'
import { instanciarQuestaoAssociacaoDeColunas } from './instanciarQuestaoAssociacaoDeColunas'
import { instanciarQuestaoVouF } from './instanciarQuestaoVouF'
import { instanciarQuestaoDiscursiva } from './instanciarQuestaoDiscursiva'
import { instanciarQuestaoRedacao } from './instanciarQuestaoRedacao'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

export const funcoesInstanciarQuestao = {
  [enumTipoQuestao.bloco]: instanciarQuestaoBloco,
  [enumTipoQuestao.multiplaEscolha]: instanciarQuestaoMultiplaEscolha,
  [enumTipoQuestao.associacaoDeColunas]: instanciarQuestaoAssociacaoDeColunas,
  [enumTipoQuestao.vouf]: instanciarQuestaoVouF,
  [enumTipoQuestao.discursiva]: instanciarQuestaoDiscursiva,
  [enumTipoQuestao.redacao]: instanciarQuestaoRedacao,
}

export const instanciarQuestao = (dados, opcoes) => {
  const questao = dados
  if (!questao) throw new Error('O objeto questao é inválido')
  const instanciar = funcoesInstanciarQuestao[questao.tipo]
  if (instanciar) {
    return instanciar(questao, opcoes)
  } else {
    throw new Error('Tipo de questão não tem função para instanciar: ' + questao.tipo)
  }
}
export const instanciarQuestoes = (questoes, opcoes) => questoes.map(dados => instanciarQuestao(dados, opcoes))
