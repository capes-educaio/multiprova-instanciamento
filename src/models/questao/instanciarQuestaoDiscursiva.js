export const instanciarQuestaoDiscursiva = questao => {
  if (!questao) throw new Error('O objeto questao é inválido')
  const { discursiva } = questao
  return {
    questaoMatrizId: questao.id,
    numeroNaMatriz: questao.numeroNaMatriz,
    numeroNaInstancia: questao.numeroNaInstancia,
    fixa: questao.fixa,
    valor: questao.valor,
    enunciado: questao.enunciado,
    dificuldade: questao.dificuldade,
    tipo: questao.tipo,
    discursiva: {
      ...discursiva,
      respostaCandidato: null,
      dataRespostaCandidato: null,
    },
  }
}
