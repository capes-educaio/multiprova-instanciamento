import { instanciarProva } from '../prova'

export const instanciarCaderno = (caderno, opcoes) => {
  const { id, titulo, template, duracao, fraseDeRodape, logoCaderno, logoComperve, instrucoes, cabecalho } = caderno
  const instanciado = { id, titulo, template, duracao, fraseDeRodape, logoCaderno, logoComperve, instrucoes, cabecalho }
  let numeroDaPrimeiraQuestao = 1
  instanciado.provas = caderno.provas.map(prova => {
    const provaInstanciada = instanciarProva(prova, { ...opcoes, numeroDaPrimeiraQuestao })
    numeroDaPrimeiraQuestao += provaInstanciada.numeroDeQuestoesNaProva
    return provaInstanciada
  })
  return instanciado
}
