import { embaralharQuestoesNaProva } from './embaralharQuestoesNaProva'
import { instanciarQuestoes } from '../questao/instanciarQuestao'
import { tipo } from 'utils/enumTipoDeEmbaralhamento'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'

const getNumeroDeQuestaoNoGrupo = grupo => {
  let numeroDeQuestoesNoGrupo = 0
  let temRedacao = false
  grupo.questoes.forEach(questao => {
    if (questao.tipo === 'bloco') numeroDeQuestoesNoGrupo += questao.bloco.questoes.length
    else if (questao.tipo === enumTipoQuestao.redacao) temRedacao = true
    else numeroDeQuestoesNoGrupo++
  })
  return { numeroDeQuestoesNoGrupo, temRedacao }
}

const getDadosProva = prova => {
  let numeroDeQuestoes = 0
  let provaTemRedacao = false
  prova.grupos.forEach(grupo => {
    const { numeroDeQuestoesNoGrupo, temRedacao } = getNumeroDeQuestaoNoGrupo(grupo)
    numeroDeQuestoes += numeroDeQuestoesNoGrupo
    if (temRedacao) provaTemRedacao = true
  })
  return { numeroDeQuestoesNaProva: numeroDeQuestoes, temRedacao: provaTemRedacao }
}

const setGrupoMetadados = grupo => ({
  ...grupo,
  numeroDeQuestoesNoGrupo: getNumeroDeQuestaoNoGrupo(grupo),
})

const setMetadados = prova => {
  const { numeroDeQuestoesNaProva, temRedacao } = getDadosProva(prova)
  return {
    titulo: prova.titulo,
    tema: prova.tema,
    instituicao: prova.instituicao,
    tipoEmbaralhamento: prova.tipoEmbaralhamento,
    tipoProva: prova.tipoProva,
    dadosAplicacao: prova.dadosAplicacao,
    nomeProfessor: prova.nomeProfessor,
    nomeCandidato: prova.nomeCandidato,
    matricula: prova.matricula,
    dataAplicacao: prova.dataAplicacao,
    vistaProvaHabilitada: prova.vistaProvaHabilitada,
    tagIds: prova.tagIds,
    grupos: prova.grupos.map(setGrupoMetadados),
    numeroDeQuestoesNaProva,
    valor: prova.valor,
    temRedacao,
  }
}

const enumerarGrupos = propriedade => (grupos, primeroNumero) => {
  let numeroAtual = primeroNumero
  grupos.forEach(grupo => {
    grupo.questoes.forEach(questao => {
      if (questao.tipo === enumTipoQuestao.bloco) {
        questao.bloco.questoes.forEach(questaoDoBloco => {
          questaoDoBloco[propriedade] = numeroAtual
          numeroAtual++
        })
      } else if (questao.tipo !== enumTipoQuestao.redacao) {
        questao[propriedade] = numeroAtual
        numeroAtual++
      }
    })
  })
  return grupos
}

export const instanciarProva = (prova, opcoes) => {
  if (opcoes.numeroDaPrimeiraQuestao === undefined) opcoes.numeroDaPrimeiraQuestao = 1
  prova = { ...prova }
  prova = setMetadados(prova)
  prova.grupos.forEach(grupo => {
    const questoes = instanciarQuestoes(grupo.questoes, opcoes)
    grupo.questoes = questoes
  })
  prova.grupos = enumerarGrupos('numeroNaMatriz')(prova.grupos, opcoes.numeroDaPrimeiraQuestao)
  if (prova.tipoEmbaralhamento !== tipo.naoEmbaralhar) prova = embaralharQuestoesNaProva(prova)
  prova.grupos = enumerarGrupos('numeroNaInstancia')(prova.grupos, opcoes.numeroDaPrimeiraQuestao)
  return prova
}
