import { tipo } from 'utils/enumTipoDeEmbaralhamento'
import { enumTipoQuestao } from 'utils/enumTipoQuestao'
import { embaralhar, embaralharComRestricao } from 'utils/embaralhar'

const funcoesDeEmbaralhamento = {
  [tipo.embaralharTodos]: embaralhar,
  [tipo.embaralharComRestricao]: embaralharComRestricao,
}

const embaralharBlocos = funcaoEmbaralhamento => grupo =>
  grupo.questoes.map(questao => {
    if (questao.tipo === enumTipoQuestao.bloco) {
      questao.bloco.questoes = funcaoEmbaralhamento(questao.bloco.questoes)
    }
    return questao
  })

const embaralharGrupo = funcaoEmbaralhamento => grupo => {
  grupo.questoes = embaralharBlocos(funcaoEmbaralhamento)(grupo)
  grupo.questoes = funcaoEmbaralhamento(grupo.questoes)
  return grupo
}

export const embaralharQuestoesNaProva = prova => {
  if (!prova) throw new Error('O objeto prova é inválido')
  let provaInstanciada = { ...prova }
  const funcaoEmbaralhamento = funcoesDeEmbaralhamento[prova.tipoEmbaralhamento]
  if (!funcaoEmbaralhamento) throw new Error('Tipo de embaralhamento não identificado: ', prova.tipoEmbaralhamento)
  provaInstanciada.grupos = prova.grupos.map(embaralharGrupo(funcaoEmbaralhamento))
  return provaInstanciada
}
