import { instanciarProva } from './instanciarProva'
import { enumStatusInstanciamento } from 'utils/enumStatusInstanciamento'

export const gerarDocumentoInstanciadoProva = ({ dados, opcoes }) => {
  if (!dados) throw new Error('O objeto prova é inválido')
  const { numeroInstancias, usuarioId, tipo, id } = dados
  const { tipoAplicacao, virtual, papel } = dados.dadosAplicacao
  const status = enumStatusInstanciamento.naoIniciada
  const documento = { status, usuarioId, tipo, idMatriz: id, tipoAplicacao, virtual, papel }
  const range = [...Array(numeroInstancias).keys()]
  documento.provas = range.map(() => instanciarProva(dados, opcoes))
  return documento
}
