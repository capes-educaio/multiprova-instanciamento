import { instanciarCaderno } from '../caderno'

export const gerarDocumentoInstanciadoCadernoConcurso = ({ dados, opcoes }) => ({
  ...instanciarCaderno(dados, opcoes),
  tipo: dados.tipo,
})
