export const sendToQueue = ({ ch, queue, mensagem }) => {
  ch.sendToQueue(queue, Buffer.from(JSON.stringify(mensagem)), { persistent: true })
  console.log(`Mandando mensagem (${mensagem.id}) para fila: ${queue}. 
  Operação (${mensagem.operacao.id}) tipo ${mensagem.operacao.tipo}`)
}
