import express from 'express'

const app = express()

app.get('/', function(req, res) {
  res.send('Bem vindo a API para instanciamento de provas =)')
})

export default app
