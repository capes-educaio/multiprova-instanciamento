# multiprova-instanciamento

[![pipeline status](https://gitlab.com/tapioca/multiprova/multiprova-instanciamento/badges/develop/pipeline.svg)](https://gitlab.com/tapioca/multiprova/multiprova-instanciamento/commits/develop)
[![quality gate](http://sonar.lii.imd.ufrn.br/dashboard?id=multiprova-instanciamento)](http://sonar.lii.imd.ufrn.br/sonar/dashboard?id=multiprova-instanciamento)
[![cobertura de testes](http://sonar.lii.imd.ufrn.br/component_measures?id=multiprova-instanciamento&metric=coverage)](http://sonar.lii.imd.ufrn.br/component_measures?id=multiprova-instanciamento&metric=coverage)
[![Bugs](http://sonar.lii.imd.ufrn.br/project/issues?id=multiprova-instanciamento&resolved=false&types=BUG)](http://sonar.lii.imd.ufrn.br/project/issues?id=multiprova-instanciamento&resolved=false&types=BUG)
[![Code Smell](http://sonar.lii.imd.ufrn.br/project/issues?id=multiprova-instanciamento&resolved=false&types=CODE_SMELL)](http://sonar.lii.imd.ufrn.br/project/issues?id=multiprova-instanciamento&resolved=false&types=CODE_SMELL)
[![Vunerabilidade](http://sonar.lii.imd.ufrn.br/project/issues?id=multiprova-instanciamento&resolved=false&types=VULNERABILITY)](http://sonar.lii.imd.ufrn.br/project/issues?id=multiprova-instanciamento&resolved=false&types=VULNERABILITY)

As solicitações e resposta para instanciamento são realizadas assincronamente através do broker [RabbitMQ](https://www.rabbitmq.com/).

## Instruções

1.  Clone o repositório.
2.  Entre no diretório: `$ cd multiprova-instanciamento`
3.  Instale as dependências: `$ npm install`
4.  Inicie o broker localmente através do seguinte comando Docker (Linux ou MacOs): `$ rmq_container_id=$(docker run -d -p 5672:5672 -p 8080:15672 --restart always rabbitmq) && sleep 10 && docker exec $rmq_container_id rabbitmq-plugins enable rabbitmq_management`
5.  No windows utilize o seguinte comando: `$ docker run -d -p 5672:5672 -p 8080:15672 --restart always rabbitmq`
6.  Inicie o serviço através do comando `$ npm start`

## Executar com Docker

Além da execução local, é possíivel também executar via Docker. Para isso, existem duas diferentes possibilidades:

1. Criar a imagem localmente e executá-la
2. Executar imagem registrada no registro do Gitlab

### Criar imagem localmente

Esta opção é útil quando se está desenvolvendo o serviço e deseja testar no mesmo ambiente que será posto em produção.
Além disso, como este projeto utiliza o Chrome headless, alguns sistemas operacionais não são plenamentes compatíveis. Nestes casos, o Docker se torna uma excelente alternativa.

Dito isso, para criar e executar a imagem localmente, siga os seguintes passos:

1. Crie a imagem com o comando: `$ docker build -t multiprova-instanciamento .`
2. Execute a imagem: `docker run -i -d --cap-add=SYS_ADMIN -e RABBIT_SERVER_URL=amqp:HOST_BROKER -p 3100:3100 multiprova-instanciamento`

Importante: Lembre-se de trocar **HOST_BROKER** pelo hostname do broker. Não utilize localhost, pois, neste caso, o serviço não será capaz de localizar o broker. Em vez disso, utilize o nome da máquina ou IP local do servidor do broker.

### Executar imagem do Gitlab

Caso precise apenas executar o serviço, esta se torna uma excelente alternativa. Neste caso, não é necessário realizar a clonagem do projeto, pois serão utilizadas as imagens registradas pelo pipeline do projeto.

Para isso, siga o seguinte passo a passo:

1. Faça login no Docker do Gitlab: `docker login registry.gitlab.com/tapioca/multiprova/multiprova-instanciamento`
2. Para executar a imagem do último pipeline de desenvolvimento: `docker run -i -d --cap-add=SYS_ADMIN -e RABBIT_SERVER_URL=amqp:HOST_BROKER -p 3100:3100 registry.gitlab.com/tapioca/multiprova/multiprova-instanciamento:dev`
3. Para executar a imagem da última versão estável: `docker run -i -d --cap-add=SYS_ADMIN -e RABBIT_SERVER_URL=amqp:HOST_BROKER -p 3100:3100 registry.gitlab.com/tapioca/multiprova/multiprova-instanciamento:latest`

Importante: Lembre-se de trocar **HOST_BROKER** pelo hostname do broker. Não utilize localhost, pois, neste caso, o serviço não será capaz de localizar o broker. Em vez disso, utilize o nome da máquina ou IP local do servidor do broker.

### Configurações disponíveis

É possível alterar uma série de parâmetros do sistema atravéz da definição de variáveis de ambiente. A tabela abaixo lista as variáveis disponíveis:

| **Variável**                         | **Descrição**                                                                                                         |
| ------------------------------------ | --------------------------------------------------------------------------------------------------------------------- |
| RABBIT_SERVER_URL                    | Url do servidor do broker RabbitMQ. Valor padrão: amqp://localhost                                                    |
| RABBIT_SERVER_TESTE_URL              | Url do servidor do broker RabbitMQ a ser utilizado no teste de integração. Valor padrão: amqp://localhost             |
| WEB_PROTOCOLO                        | Protocolo (http ou https) a ser utilizado para o download dos arquivos. Valor padrão: http                            |
| WEB_PORT                             | Porta a ser utilizada para servir os arquivos renderizados. Valor padrão: 3000                                        |
| API_HOSTNAME                         | Hostname do host a ser utilizado para baixar os arquivos. Valor padrão: localhost                                     |
| TEMPO_EXPIRACAO_POS_DOWNLOAD_MINUTOS | Tempo em minutos a ser aguardado após o download do arquivo para o mesmo ser removido em definitivo. Valor padrão: 10 |
| TEMPO_EXPIRACAO_MINUTOS              | Tempo em minutos a ser aguardado, independente de ocorrer o download, para a remoção do arquivo. Valor padrão: 20     |
| MONITORAMENTO_EXPIRACAO_MINUTOS      | Período de tempo (em minutos) para checagem se há arquivos a serem removidos. Valor padrão: 2                         |

## Administração do Broker

Caso tenha executado o broker localmente e queira monitorá-lo, acesse o painel de administração através do seguinte endereço: http://localhost:8080/

Utilize as seguintes credenciais:

- Usuário: guest
- Senha: guest
