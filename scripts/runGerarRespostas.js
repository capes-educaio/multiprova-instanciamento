const { getMensagemResposta } = require('getMensagemResposta')

const editaJson = require('edit-json-file')

let { argv } = process

const nomeEntrada = argv[2]
const nomeSaida = argv[3]
const pathEntrada = `../samples/${nomeEntrada}.json`
const pathSaida = nomeSaida ? `samples/${nomeSaida}.json` : `samples/${nomeEntrada}-resposta.json`

const conteudoEntrada = require(pathEntrada)
const respostasArquivo = editaJson(pathSaida)

for (let id in conteudoEntrada) {
  const mensagem = conteudoEntrada[id]
  const resposta = getMensagemResposta(mensagem)
  respostasArquivo.set(resposta.id, resposta)
}

respostasArquivo.save()
