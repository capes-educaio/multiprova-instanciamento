var shell = require('shelljs')

let { argv } = process

process.env.BABEL_ENV = 'test'
process.env.NODE_ENV = 'test'
process.env.NODE_PATH = 'src'

let runner = 'babel-node'

const args = argv.slice(2).join(' ')

shell.exec(`${runner} scripts/runGerarRespostas ${args}`)
