import * as vouf from 'models/questao/instanciarQuestaoVouF'
import {
  questaoVouF,
  questaoVouFInstanciada,
  questaoVouFInstanciadaEmbaralhada,
} from '../data/instanciasModeloQuestaoVouF'
describe('testando instancia de questoes do tipo vouf', () => {
  test('instanciando sem embaralhamento', () => {
    const questaoInstanciada = vouf.instanciarQuestaoVouF(questaoVouF, false)
    expect(questaoInstanciada).toEqual(questaoVouFInstanciada)
  })
  test('instanciando e embaralhamento', () => {
    const questaoInstanciadaAfirmacoes = vouf.instanciarEembaralharAfirmacoes(
      questaoVouF.vouf.afirmacoes
    )
    questaoVouFInstanciadaEmbaralhada.vouf.afirmacoes.forEach(afirmacao =>
      expect(questaoInstanciadaAfirmacoes).toContainEqual(afirmacao)
    )
    questaoInstanciadaAfirmacoes.forEach(afirmacao =>
      expect(questaoVouFInstanciadaEmbaralhada.vouf.afirmacoes).toContainEqual(
        afirmacao
      )
    )
  })
  test('testando instanciamento de afirmações', () => {
    const afirmacoes = vouf.instanciarAfirmacoes(questaoVouF.vouf.afirmacoes)
    expect(afirmacoes).toEqual(questaoVouFInstanciada.vouf.afirmacoes)
  })
  test('testando o embaralhamento', () => {
    const afirmacoes = vouf.embaralharAfirmacoes(
      questaoVouFInstanciada.vouf.afirmacoes
    )

    questaoVouFInstanciadaEmbaralhada.vouf.afirmacoes.forEach(afirmacao =>
      expect(afirmacoes).toContainEqual(afirmacao)
    )
    afirmacoes.forEach(afirmacao =>
      expect(questaoVouFInstanciadaEmbaralhada.vouf.afirmacoes).toContainEqual(
        afirmacao
      )
    )
  })
})
