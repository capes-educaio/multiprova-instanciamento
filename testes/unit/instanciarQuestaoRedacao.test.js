import * as redacao from 'models/questao/instanciarQuestaoRedacao'
import {
  questaoRedacao,
  questaoRedacaoInstanciada,
} from '../data/instanciasModeloQuestaoRedacao'
describe('testando instancia de questoes do tipo redacao', () => {
  test('instanciando', () => {
    const questaoInstanciada = redacao.instanciarQuestaoRedacao(questaoRedacao)
    expect(questaoInstanciada).toEqual(questaoRedacaoInstanciada)
  })
})
