import { embaralhar } from 'utils/embaralhar'

describe('embaralhar', () => {
  test('Embaralhamento eventualmente cria um array diferente', () => {
    const array = ['a', 'b', 'c']
    let arrayEmbaralhado = embaralhar(array)
    while (JSON.stringify(arrayEmbaralhado) === JSON.stringify(array)) arrayEmbaralhado = embaralhar(array)
  })
})
