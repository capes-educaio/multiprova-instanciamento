import * as associacaoDeColunas from 'models/questao/instanciarQuestaoAssociacaoDeColunas'
import {
  questaoAssociacaoDeColunas,
  questaoAssociacaoDeColunasInstanciada,
  questaoAssociacaoDeColunasEmbaralhada,
} from '../data/instanciasModeloQuestaoAssociacaoDeColunas'

const mockMath = Object.create(global.Math)
mockMath.random = () => 0.4
global.Math = mockMath

describe('Instanciar Questão Associação de Colunas', () => {
  test('Testa instanciamento sem embaralhamento', () => {
    const colunaAInstanciada = associacaoDeColunas.instanciarColunaA(
      questaoAssociacaoDeColunas.associacaoDeColunas.colunaA
    )
    expect(colunaAInstanciada).toEqual(
      questaoAssociacaoDeColunasInstanciada.associacaoDeColunas.colunaA
    )

    const colunaBInstanciadas = associacaoDeColunas.instanciarColunaB(
      questaoAssociacaoDeColunas.associacaoDeColunas.colunaB
    )
    expect(colunaBInstanciadas).toEqual(
      questaoAssociacaoDeColunasInstanciada.associacaoDeColunas.colunaB
    )
  })
  test('Testa instanciamento e embaralhamento da coluna A da questão', () => {
    let colunaAInstanciada = associacaoDeColunas.instanciarColunaA(
      questaoAssociacaoDeColunas.associacaoDeColunas.colunaA
    )

    colunaAInstanciada = associacaoDeColunas.embaralharColuna(
      colunaAInstanciada,
      associacaoDeColunas.trocarConteudoItens
    )

    expect(colunaAInstanciada).toEqual(
      questaoAssociacaoDeColunasEmbaralhada.associacaoDeColunas.colunaA
    )
  })
  test('Testa instanciamento e embaralhamento da coluna B da questão', () => {
    let colunaBInstanciada = associacaoDeColunas.instanciarColunaB(
      questaoAssociacaoDeColunas.associacaoDeColunas.colunaB
    )
    colunaBInstanciada = associacaoDeColunas.embaralharColuna(
      colunaBInstanciada,
      associacaoDeColunas.trocarItens
    )
    questaoAssociacaoDeColunasEmbaralhada.associacaoDeColunas.colunaB.forEach(
      item => expect(colunaBInstanciada).toContainEqual(item)
    )
  })
})
