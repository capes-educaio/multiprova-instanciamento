import * as discursiva from 'models/questao/instanciarQuestaoDiscursiva'
import {
  questaoDiscursiva,
  questaoDiscursivaInstanciada,
} from '../data/instanciasModeloQuestaoDiscursiva'
describe('testando instancia de questoes do tipo discursiva', () => {
  test('instanciando', () => {
    const questaoInstanciada = discursiva.instanciarQuestaoDiscursiva(
      questaoDiscursiva
    )
    expect(questaoInstanciada).toEqual(questaoDiscursivaInstanciada)
  })
})
