import { dadosIguais } from './instanciasModeloQuestao'

export const questaoDiscursiva = {
  id: '1',
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  enunciado: 'Qual a <b>soma</b>  de 2+2?',
  dificuldade: 2,
  tipo: 'discursiva',
  discursiva: {
    expectativaDeResposta: 'resposta aqui',
    numeroDeLinhas: 10,
  },
  tagsId: [],
}

export const questaoDiscursivaInstanciada = {
  ...dadosIguais,
  tipo: 'discursiva',
  discursiva: {
    expectativaDeResposta: 'resposta aqui',
    numeroDeLinhas: 10,
    respostaCandidato: null,
    dataRespostaCandidato: null,
  },
  fixa: undefined,
}
