import { dadosIguais } from './instanciasModeloQuestao'

export const questaoVouF = {
  id: '1',
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  enunciado: 'Qual a <b>soma</b>  de 2+2?',
  dificuldade: 2,
  tipo: 'vouf',
  vouf: {
    afirmacoes: [
      {
        texto: '<p>PRIMEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 0,
      },
      {
        texto: '<p>SEGUNDA F</p>',
        letra: 'F',
        posicaoNaMatriz: 1,
      },
      {
        texto: '<p>TERCEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 2,
      },
    ],
  },
  tagsId: [],
}

export const questaoVouFInstanciada = {
  ...dadosIguais,
  tipo: 'vouf',
  vouf: {
    afirmacoes: [
      {
        texto: '<p>PRIMEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 0,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        texto: '<p>SEGUNDA F</p>',
        letra: 'F',
        posicaoNaMatriz: 1,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        texto: '<p>TERCEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 2,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
    ],
  },
  fixa: undefined,
}

export const questaoVouFInstanciadaEmbaralhada = {
  ...dadosIguais,
  tipo: 'vouf',
  vouf: {
    afirmacoes: [
      {
        texto: '<p>SEGUNDA F</p>',
        letra: 'F',
        posicaoNaMatriz: 1,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        texto: '<p>TERCEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 2,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        texto: '<p>PRIMEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 0,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
    ],
  },
  fixa: undefined,
}
