import { tipo } from 'utils/enumTipoDeEmbaralhamento'

/* Constantes para testar o gabarito */

export var numeroDaQuestao = {
  numero: 1,
  get atual() {
    return this.numero
  },
  set atual(value) {
    this.numero = value
  },
}

const camposComunsMultiplaEscolha = {
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  enunciado: 'Qual a <b>soma</b>  de 2+2?',
  dificuldade: 2,
  tipo: 'multiplaEscolha',
  multiplaEscolha: {
    respostaCerta: 'a',
    alternativas: [
      {
        texto: 'A soma vale 4',
        letra: 'a',
      },
      {
        texto: 'A soma vale 5',
        letra: 'b',
      },
      {
        texto: 'A soma vale 6',
        letra: 'c',
      },
    ],
    alternativasPorLinha: 1,
  },
  tagsId: [],
}

const camposComunsVouF = {
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  enunciado: 'Qual a <b>soma</b>  de 2+2?',
  dificuldade: 2,
  tipo: 'vouf',
  vouf: {
    afirmacoes: [
      {
        texto: '<p>PRIMEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 0,
      },
      {
        texto: '<p>SEGUNDA F</p>',
        letra: 'F',
        posicaoNaMatriz: 1,
      },
      {
        texto: '<p>TERCEIRA V</p>',
        letra: 'V',
        posicaoNaMatriz: 2,
      },
    ],
  },
  tagsId: [],
}

const camposComunsAssociacaoDeColunas = {
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  enunciado: '<p>Qual a <b>soma</b>  de 2+2?</p>',
  dificuldade: 2,
  tipo: 'associacaoDeColunas',
  associacaoDeColunas: {
    colunaA: [
      {
        rotulo: 1,
        conteudo: '<p>substantivo</p>',
      },
      {
        rotulo: 2,
        conteudo: '<p>adjetivo</p>',
      },
    ],
    colunaB: [
      {
        rotulo: 2,
        conteudo: '<p>Feio</p>',
      },
      {
        rotulo: 1,
        conteudo: '<p>Casa</p>',
      },
      {
        rotulo: 1,
        conteudo: '<p>João</p>',
      },
      {
        rotulo: 2,
        conteudo: '<p>Bonito</p>',
      },
    ],
  },
  tagsId: [],
}

const camposComunsBloco = {
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  tipo: 'bloco',
  enunciado: 'Era uma vez... ... e viveram felizes para sempre',
}

const camposComunsProva = {
  descricao:
    'Prova de cálculo numérido para a segunda turma de Eng. de Materiais',
  titulo: 'Prova de Matemática',
  tema: 'Integrais duplas',
  instituicao: 'UFRN',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  dataAplicacao: '2018-06-23T18:52:53.085Z',
  tagsId: [],
  nomeProfessor: 'Nome do Professor',
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
}

export const questaoMultiplaEscolha = (id = 1) => ({
  id,
  ...camposComunsMultiplaEscolha,
  fixa: false,
})

export const questaoBloco = (id = 10) => ({
  id,
  ...camposComunsBloco,
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 8 à 11.',
    questoes: [
      questaoMultiplaEscolha(1),
      questaoMultiplaEscolha(2),
      questaoMultiplaEscolha(3),
    ],
  },
  fixa: false,
})

export const grupo1 = {
  nome: 'Nome do grupo1',
  questoes: [questaoMultiplaEscolha(), questaoBloco()],
}

export const grupo2 = {
  nome: 'Nome do grupo2',
  questoes: [questaoBloco(), questaoMultiplaEscolha()],
}

export const prova = {
  id: '1',
  ...camposComunsProva,
  tipoEmbaralhamento: tipo.EMBARALHAR_TODOS,
  grupos: [grupo1, grupo2],
}

export const questaoMultiplaEscolhaGabaritada = (id = 1) => {
  const num = numeroDaQuestao.atual
  numeroDaQuestao.atual++
  return {
    id,
    ...camposComunsMultiplaEscolha,
    numeroNaMatriz: num,
    numeroNaInstancia: num,
    fixa: false,
  }
}

export const questaoBlocoGabaritada = (id = 10) => ({
  id,
  ...camposComunsBloco,
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 8 à 11.',
    questoes: [
      questaoMultiplaEscolhaGabaritada(1),
      questaoMultiplaEscolhaGabaritada(2),
      questaoMultiplaEscolhaGabaritada(3),
    ],
  },
  fixa: false,
})

export const grupo1Gabaritado = {
  nome: 'Nome do grupo1',
  questoes: [questaoMultiplaEscolhaGabaritada(), questaoBlocoGabaritada()],
  numeroDeQuestoesNoGrupo: 4,
}

export const grupo2Gabaritado = {
  nome: 'Nome do grupo2',
  questoes: [questaoBlocoGabaritada(), questaoMultiplaEscolhaGabaritada()],
  numeroDeQuestoesNoGrupo: 4,
}

export const provaGabaritada = {
  id: '1',
  ...camposComunsProva,
  tipoEmbaralhamento: tipo.EMBARALHAR_TODOS,
  grupos: [grupo1Gabaritado, grupo2Gabaritado],
  numeroDeQuestoesNaProva: 8,
}

export const gabarito = [
  {
    numeroDaQuestao: 1,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
  {
    numeroDaQuestao: 2,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
  {
    numeroDaQuestao: 3,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
  {
    numeroDaQuestao: 4,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
  {
    numeroDaQuestao: 5,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
  {
    numeroDaQuestao: 6,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
  {
    numeroDaQuestao: 7,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
  {
    numeroDaQuestao: 8,
    respostaCerta: 'c',
    numAlternativas: 3,
  },
]

/* Constantes para testar o instanciamento e embaralhamento */

export const questaoMultiplaEscolhaEmbaralhar = (
  id,
  numeroNaMatriz,
  numeroNaInstancia,
  fixa
) => {
  return {
    id,
    ...camposComunsMultiplaEscolha,
    numeroNaMatriz,
    numeroNaInstancia,
    fixa,
  }
}

export const questaoVouFEmbaralhar = (
  id,
  numeroNaMatriz,
  numeroNaInstancia,
  fixa
) => {
  return {
    id,
    ...camposComunsVouF,
    numeroNaMatriz,
    numeroNaInstancia,
    fixa,
  }
}

export const questaoAssociacaoDeColunasEmbaralhar = (
  id,
  numeroNaMatriz,
  numeroNaInstancia,
  fixa
) => {
  return {
    id,
    ...camposComunsAssociacaoDeColunas,
    numeroNaMatriz,
    numeroNaInstancia,
    fixa,
  }
}

export const questaoBlocoTrocadaSoAsDuasPrimeiras = () => ({
  id: '1',
  ...camposComunsBloco,
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 1 à 4.',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(2, 2, 1, false),
      questaoMultiplaEscolhaEmbaralhar(1, 1, 2, false),
      questaoMultiplaEscolhaEmbaralhar(3, 3, 3, true),
      questaoMultiplaEscolhaEmbaralhar(4, 4, 4, false),
    ],
  },
})

export const questaoBlocoEmbaralhada = (
  id = 1,
  primeiroNumeroNaMatriz = 1,
  primeiroNumeroNaInstancia = 1,
  fixa = false
) => ({
  id,
  ...camposComunsBloco,
  fixa,
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 1 à 4.',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(
        3,
        primeiroNumeroNaMatriz + 2,
        primeiroNumeroNaInstancia,
        true
      ),
      questaoMultiplaEscolhaEmbaralhar(
        1,
        primeiroNumeroNaMatriz,
        primeiroNumeroNaInstancia + 1,
        false
      ),
      questaoMultiplaEscolhaEmbaralhar(
        4,
        primeiroNumeroNaMatriz + 3,
        primeiroNumeroNaInstancia + 2,
        false
      ),
      questaoMultiplaEscolhaEmbaralhar(
        2,
        primeiroNumeroNaMatriz + 1,
        primeiroNumeroNaInstancia + 3,
        false
      ),
    ],
  },
})

export const questaoBlocoAntesDeEmbaralhar = (
  id = 1,
  primeiroNumeroNaMatriz = 1,
  primeiroNumeroNaInstancia = 1,
  fixa = false
) => ({
  id: id,
  ...camposComunsBloco,
  fixa,
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 1 à 4.',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(
        1,
        primeiroNumeroNaMatriz,
        primeiroNumeroNaInstancia,
        false
      ),
      questaoMultiplaEscolhaEmbaralhar(
        2,
        primeiroNumeroNaMatriz + 1,
        primeiroNumeroNaInstancia + 1,
        false
      ),
      questaoMultiplaEscolhaEmbaralhar(
        3,
        primeiroNumeroNaMatriz + 2,
        primeiroNumeroNaInstancia + 2,
        true
      ),
      questaoMultiplaEscolhaEmbaralhar(
        4,
        primeiroNumeroNaMatriz + 3,
        primeiroNumeroNaInstancia + 3,
        false
      ),
    ],
  },
})

export const questaoBlocoEmbaralhadaRestricao = (
  id = 1,
  primeiroNumeroNaMatriz = 1,
  primeiroNumeroNaInstancia = 1,
  fixa = false
) => ({
  id,
  ...camposComunsBloco,
  fixa,
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 1 à 4.',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(
        4,
        primeiroNumeroNaMatriz + 3,
        primeiroNumeroNaInstancia,
        false
      ),
      questaoMultiplaEscolhaEmbaralhar(
        1,
        primeiroNumeroNaMatriz,
        primeiroNumeroNaInstancia + 1,
        false
      ),
      questaoMultiplaEscolhaEmbaralhar(
        3,
        primeiroNumeroNaMatriz + 2,
        primeiroNumeroNaInstancia + 2,
        true
      ),
      questaoMultiplaEscolhaEmbaralhar(
        2,
        primeiroNumeroNaMatriz + 1,
        primeiroNumeroNaInstancia + 3,
        false
      ),
    ],
  },
})

export const grupoOrdenado = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoMultiplaEscolhaEmbaralhar(1, 1, 1, false),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 2, false),
    questaoBlocoAntesDeEmbaralhar(3, 3, 3, false),
    questaoMultiplaEscolhaEmbaralhar(4, 7, 7, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const grupoSoBlocoEmbaralhado = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoMultiplaEscolhaEmbaralhar(1, 1, 1, false),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 2, false),
    questaoBlocoEmbaralhada(3, 3, 3, false),
    questaoMultiplaEscolhaEmbaralhar(4, 7, 7, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const grupoEBlocoEmbaralhados = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoBlocoEmbaralhada(3, 3, 1, false),
    questaoMultiplaEscolhaEmbaralhar(1, 1, 5, false),
    questaoMultiplaEscolhaEmbaralhar(4, 7, 6, false),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 7, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const grupoEmbaralhadoMasBlocoNao = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoBlocoAntesDeEmbaralhar(3, 3, 1, false),
    questaoMultiplaEscolhaEmbaralhar(1, 1, 5, false),
    questaoMultiplaEscolhaEmbaralhar(4, 7, 6, false),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 7, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const grupoEmbaralhadoEDesordenado = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoBlocoAntesDeEmbaralhar(3, 3, 3, false),
    questaoMultiplaEscolhaEmbaralhar(1, 1, 1, false),
    questaoMultiplaEscolhaEmbaralhar(4, 7, 7, false),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 2, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const grupoEmbaralhadoEOrdenado = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoBlocoAntesDeEmbaralhar(3, 3, 1, false),
    questaoMultiplaEscolhaEmbaralhar(1, 1, 5, false),
    questaoMultiplaEscolhaEmbaralhar(4, 7, 6, false),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 7, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const grupoOrdenadoComRestricao = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoMultiplaEscolhaEmbaralhar(1, 1, 1, false),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 2, false),
    questaoBlocoAntesDeEmbaralhar(3, 3, 3, true),
    questaoMultiplaEscolhaEmbaralhar(4, 7, 7, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const grupoEBlocoEmbaralhadosComRestricao = () => ({
  nome: 'Nome do grupo1',
  questoes: [
    questaoMultiplaEscolhaEmbaralhar(4, 7, 1, false),
    questaoMultiplaEscolhaEmbaralhar(1, 1, 2, false),
    questaoBlocoEmbaralhadaRestricao(3, 3, 3, true),
    questaoMultiplaEscolhaEmbaralhar(2, 2, 7, false),
  ],
  numeroDeQuestoesNoGrupo: 7,
})

export const arrayDeDoisGruposOrdenadosComRestricao = () => [
  {
    nome: 'Nome do grupo1',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(1, 1, 1, false),
      questaoMultiplaEscolhaEmbaralhar(2, 2, 2, false),
      questaoBlocoAntesDeEmbaralhar(3, 3, 3, true),
      questaoMultiplaEscolhaEmbaralhar(4, 7, 7, false),
    ],
    numeroDeQuestoesNoGrupo: 7,
  },
  {
    nome: 'Nome do grupo2',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(1, 1, 1, true),
      questaoMultiplaEscolhaEmbaralhar(2, 2, 2, false),
      questaoBlocoAntesDeEmbaralhar(3, 3, 3, false),
      questaoMultiplaEscolhaEmbaralhar(4, 7, 7, false),
    ],
    numeroDeQuestoesNoGrupo: 7,
  },
]

export const arrayDeDoisgruposEBlocoEmbaralhadosComRestricao = () => [
  {
    nome: 'Nome do grupo1',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(4, 7, 1, false),
      questaoMultiplaEscolhaEmbaralhar(1, 1, 2, false),
      questaoBlocoEmbaralhadaRestricao(3, 3, 3, true),
      questaoMultiplaEscolhaEmbaralhar(2, 2, 7, false),
    ],
    numeroDeQuestoesNoGrupo: 7,
  },
  {
    nome: 'Nome do grupo2',
    questoes: [
      questaoMultiplaEscolhaEmbaralhar(1, 1, 1, true),
      questaoBlocoEmbaralhadaRestricao(3, 3, 2, false),
      questaoMultiplaEscolhaEmbaralhar(4, 7, 6, false),
      questaoMultiplaEscolhaEmbaralhar(2, 2, 7, false),
    ],
    numeroDeQuestoesNoGrupo: 7,
  },
]

export const provaOrdenada = () => ({
  id: '1',
  ...camposComunsProva,
  tipoEmbaralhamento: tipo.EMBARALHAR_COM_RESTRICAO,
  grupos: arrayDeDoisGruposOrdenadosComRestricao(),
  numeroDeQuestoesNaProva: 14,
})

export const provaEmbaralhada = () => ({
  id: '1',
  ...camposComunsProva,
  tipoEmbaralhamento: tipo.EMBARALHAR_COM_RESTRICAO,
  grupos: arrayDeDoisgruposEBlocoEmbaralhadosComRestricao(),
  numeroDeQuestoesNaProva: 14,
})
