export const dadosIguais = {
  questaoMatrizId: '1',
  numeroNaMatriz: undefined,
  numeroNaInstancia: undefined,
  enunciado: 'Qual a <b>soma</b>  de 2+2?',
  dificuldade: 2,
}

export const questaoMultiplaEscolha = {
  id: '1',
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  enunciado: 'Qual a <b>soma</b>  de 2+2?',
  dificuldade: 2,
  tipo: 'multiplaEscolha',
  multiplaEscolha: {
    respostaCerta: 'c',
    alternativas: [
      {
        texto: 'A soma vale 4',
        letra: 'a',
      },
      {
        texto: 'A soma vale 5',
        letra: 'b',
      },
      {
        texto: 'A soma vale 6',
        letra: 'c',
      },
      {
        texto: 'A soma vale 8',
        letra: 'd',
      },
    ],
    alternativasPorLinha: 1,
  },
  tagsId: [],
}

export const questaoMultiplaEscolhaEmbaralhada = {
  ...dadosIguais,
  tipo: 'multiplaEscolha',
  multiplaEscolha: {
    respostaCandidato: null,
    dataRespostaCandidato: null,
    respostaCerta: 'c',
    alternativas: [
      {
        texto: 'A soma vale 8',
        letraNaInstancia: 'a',
        letraNaMatriz: 'd',
      },
      {
        texto: 'A soma vale 6',
        letraNaInstancia: 'b',
        letraNaMatriz: 'c',
      },
      {
        texto: 'A soma vale 4',
        letraNaInstancia: 'c',
        letraNaMatriz: 'a',
      },
      {
        texto: 'A soma vale 5',
        letraNaInstancia: 'd',
        letraNaMatriz: 'b',
      },
    ],
    alternativasPorLinha: 1,
  },
  fixa: undefined,
}

export const questaoBloco = {
  id: '2',
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  tipo: 'bloco',
  enunciado: 'Era uma vez... ... e viveram felizes para sempre',
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 8 à 11.',
    questoes: [questaoMultiplaEscolha, questaoMultiplaEscolha, questaoMultiplaEscolha],
  },
  fixa: undefined,
}

export const questaoBlocoInstanciada = {
  questaoMatrizId: '2',
  tipo: 'bloco',
  enunciado: 'Era uma vez... ... e viveram felizes para sempre',
  bloco: {
    fraseInicial: 'O texto a seguir será usado nas questões 8 à 11.',
    questoes: new Array(3).fill(questaoMultiplaEscolhaEmbaralhada),
  },
  fixa: undefined,
}
