import { dadosIguais } from './instanciasModeloQuestao'

export const questaoAssociacaoDeColunas = {
  id: '568c28fffc4be30d44d00989',
  dataCadastro: '2018-06-23T18:52:53.085Z',
  dataUltimaAlteracao: '2018-06-23T18:52:53.085Z',
  criadoPor: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  usuarioId: 'dccc93c2-41db-4aa8-a415-f4712e9fce10',
  enunciado: '<p>Qual a <b>soma</b>  de 2+2?</p>',
  dificuldade: 2,
  tipo: 'associacaoDeColunas',
  associacaoDeColunas: {
    colunaA: [
      {
        rotulo: 1,
        conteudo: '<p>substantivo</p>',
      },
      {
        rotulo: 2,
        conteudo: '<p>adjetivo</p>',
      },
    ],
    colunaB: [
      {
        rotulo: 2,
        conteudo: '<p>Feio</p>',
      },
      {
        rotulo: 1,
        conteudo: '<p>Casa</p>',
      },
      {
        rotulo: 1,
        conteudo: '<p>João</p>',
      },
      {
        rotulo: 2,
        conteudo: '<p>Bonito</p>',
      },
    ],
  },
  tagsId: [],
}

export const questaoAssociacaoDeColunasInstanciada = {
  ...dadosIguais,
  fixa: undefined,
  tipo: 'associacaoDeColunas',
  associacaoDeColunas: {
    colunaA: [
      {
        conteudo: '<p>substantivo</p>',
        rotulo: 1,
        rotuloMatriz: 1,
        posicaoMatriz: 0,
      },
      {
        conteudo: '<p>adjetivo</p>',
        rotulo: 2,
        rotuloMatriz: 2,
        posicaoMatriz: 1,
      },
    ],
    colunaB: [
      {
        conteudo: '<p>Feio</p>',
        rotuloMatriz: 2,
        posicaoMatriz: 0,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>Casa</p>',
        rotuloMatriz: 1,
        posicaoMatriz: 1,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>João</p>',
        rotuloMatriz: 1,
        posicaoMatriz: 2,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>Bonito</p>',
        rotuloMatriz: 2,
        posicaoMatriz: 3,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
    ],
  },
  tagsId: [],
}

export const questaoAssociacaoDeColunasEmbaralhada = {
  ...dadosIguais,
  fixa: undefined,
  tipo: 'associacaoDeColunas',
  associacaoDeColunas: {
    colunaA: [
      {
        conteudo: '<p>adjetivo</p>',
        rotulo: 1,
        rotuloMatriz: 2,
        posicaoMatriz: 1,
      },
      {
        conteudo: '<p>substantivo</p>',
        rotulo: 2,
        rotuloMatriz: 1,
        posicaoMatriz: 0,
      },
    ],
    colunaB: [
      {
        conteudo: '<p>João</p>',
        rotuloMatriz: 1,
        posicaoMatriz: 2,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>Feio</p>',
        rotuloMatriz: 2,
        posicaoMatriz: 0,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>Bonito</p>',
        rotuloMatriz: 2,
        posicaoMatriz: 3,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
      {
        conteudo: '<p>Casa</p>',
        rotuloMatriz: 1,
        posicaoMatriz: 1,
        respostaCandidato: null,
        dataRespostaCandidato: null,
      },
    ],
  },
  tagsId: [],
}
