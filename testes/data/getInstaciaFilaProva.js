import exemploProvaVirtual from './exemplo-prova-virtual'
import exemploProvaPapel from './exemplo-prova-papel'
import exemploProvaElaboracao from './exemplo-prova-elaboracao'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

export const getInstaciaFilaProvaVirtual = () => {
  return {
    id: 45678,
    template: enumTipoTemplate.provaComperve,
    dados: exemploProvaVirtual,
  }
}

export const getInstaciaFilaProvaPapel = () => {
  return {
    id: 56789,
    template: enumTipoTemplate.provaComperve,
    dados: exemploProvaPapel,
  }
}

export const getInstaciaFilaProvaElaboracao = () => {
  return {
    id: 123456,
    template: enumTipoTemplate.provaComperve,
    dados: exemploProvaElaboracao,
  }
}
