import {
  getInstaciaFilaProvaVirtual,
  getInstaciaFilaProvaPapel,
  getInstaciaFilaProvaElaboracao,
} from '../data/getInstaciaFilaProva'
import amqp from 'amqplib/callback_api'
import config from 'config'

const FILA = 'request-created-instances'

export function enviarMensagemTesteVirtual(idReq, callback) {
  console.log(
    `irá enviar uma mensagem com prova virtual para a FILA ${
      config.rabbit.server_test_url
    }`
  )
  amqp.connect(
    config.rabbit.server_test_url,
    function(err, conexao) {
      conexao.createConfirmChannel(function(err, canal) {
        canal.assertQueue(FILA, { durable: true })
        const instanciaFilaProva = getInstaciaFilaProvaVirtual()
        instanciaFilaProva.id = idReq
        canal.sendToQueue(
          FILA,
          Buffer.from(JSON.stringify(instanciaFilaProva)),
          { persistent: true },
          function(err, ok) {
            canal.close(function(err) {
              conexao.close(function(err) {
                callback()
              })
            })
          }
        )
      })
    }
  )
}

export function enviarMensagemTestePapel(idReq, callback) {
  console.log(
    `irá enviar uma mensagem com prova em papel para a FILA ${
      config.rabbit.server_test_url
    }`
  )
  amqp.connect(
    config.rabbit.server_test_url,
    function(err, conexao) {
      conexao.createConfirmChannel(function(err, canal) {
        canal.assertQueue(FILA, { durable: true })
        const instanciaFilaProva = getInstaciaFilaProvaPapel()
        instanciaFilaProva.id = idReq
        canal.sendToQueue(
          FILA,
          Buffer.from(JSON.stringify(instanciaFilaProva)),
          { persistent: true },
          function(err, ok) {
            canal.close(function(err) {
              conexao.close(function(err) {
                callback()
              })
            })
          }
        )
      })
    }
  )
}

export function enviarMensagemTestePreview(idReq, callback) {
  console.log(
    `irá enviar uma mensagem com prova em preview para a FILA ${
      config.rabbit.server_test_url
    }`
  )
  amqp.connect(
    config.rabbit.server_test_url,
    function(err, conexao) {
      conexao.createConfirmChannel(function(err, canal) {
        canal.assertQueue(FILA, { durable: true })
        const instanciaFilaProva = getInstaciaFilaProvaElaboracao()
        instanciaFilaProva.id = idReq
        canal.sendToQueue(
          FILA,
          Buffer.from(JSON.stringify(instanciaFilaProva)),
          { persistent: true },
          function(err, ok) {
            canal.close(function(err) {
              conexao.close(function(err) {
                callback()
              })
            })
          }
        )
      })
    }
  )
}
