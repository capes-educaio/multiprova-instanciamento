import amqp from 'amqplib/callback_api'
import config from 'config'

const URL_RABBIT = config.rabbit.server_url

export class ConsumidorTeste {
  constructor(fila) {
    this.fila = fila
  }

  conexao = null
  gerador = null
  canal = null
  dadosDaFila = null

  iniciarConsumidor(sucess, error) {
    console.log(
      '(ConsumidorTeste) Conectando ao servidor do RabbitMQ no seguinte endereço: %s',
      URL_RABBIT
    )
    console.log(
      '(ConsumidorTeste) Inicilizando o um consumidor para a fila %s.',
      this.fila
    )
    amqp.connect(
      URL_RABBIT,
      (err, conexao) => {
        if (err) {
          console.error(
            '(ConsumidorTeste) Falha ao tentar se conectar com o broker na seguinte URL ' +
              URL_RABBIT +
              '(ConsumidorTeste) O serviço do Rabbit está rodando? Cheque o serviço se a URL e porta estão corretos.'
          )
          error(err)
        } else {
          this.conexao = conexao
          console.log(
            '(ConsumidorTeste) Conectado ao servidor do Rabbit para a fila %s. Irá criar o canal.',
            this.fila
          )
          this.criarCanal(conexao, sucess, error)
        }
      }
    )
  }

  shutdownConsumidor() {
    console.log(`(ConsumidorTeste) Desligando o consumidor.`)
    if (this.conexao) this.conexao.close()
    else
      console.error('(ConsumidorTeste) Tentando desligar conexao não existente')
  }

  criarCanal(conexao, sucess, error) {
    conexao.createChannel((erro, canal) => {
      if (erro) {
        error(erro)
      } else {
        this.canal = canal
        console.log(
          '(ConsumidorTeste) Canal de comunicação criado para a fila %s.',
          this.fila
        )
        canal.assertQueue(this.fila, { durable: true })
        canal.prefetch(1)
        console.log(
          '(ConsumidorTeste) Aguardando mensagens para a fila %s.',
          this.fila
        )
        this.registrarConsumidor(this.canal)
        sucess()
      }
    })
  }

  registrarConsumidor() {
    this.canal.consume(this.fila, this.consumir, {
      noAck: false,
    })
  }

  consumir = instanciaFila => {
    const content = JSON.parse(instanciaFila.content)
    this.dadosDaFila = content.dados
    this.limparInstancia(instanciaFila)
  }

  limparInstancia = instanciaFila => {
    this.canal.ack(instanciaFila)
  }
}
