import { Consumidor } from 'consumidor'
import { ConsumidorTeste } from './consumidor-teste'
import { enviarMensagemTesteVirtual, enviarMensagemTestePapel } from './rabbit-cliente'

const QUEUE_RESPONSE_INSTANCIAMENTO = 'response-created-instances'
const QUEUE_REQUEST_IMPRESSAO = 'request-render-pdf'

let consumidor, consumidorTesteInstanciamento, consumidorTesteImpressao

beforeEach(async done => {
  consumidor = new Consumidor()
  consumidorTesteInstanciamento = new ConsumidorTeste(QUEUE_RESPONSE_INSTANCIAMENTO)
  consumidorTesteImpressao = new ConsumidorTeste(QUEUE_REQUEST_IMPRESSAO)
  consumidor.iniciarConsumidor(
    function() {
      done()
    },
    function(err) {
      console.error(`Falha durante a inicialização do consumidor. O serviço será encerrado. ${err}`)
      done()
    }
  )
  consumidorTesteImpressao.iniciarConsumidor(
    function() {
      done()
    },
    function(err) {
      console.error(`Falha durante a inicialização do consumidorTesteImpressao. O serviço será encerrado. ${err}`)
      done()
    }
  )
  consumidorTesteInstanciamento.iniciarConsumidor(
    function() {
      done()
    },
    function(err) {
      console.error(`Falha durante a inicialização do consumidorTesteInstanciamento. O serviço será encerrado. ${err}`)
      done()
    }
  )
})

afterEach(async done => {
  console.log('desligando...')
  await consumidor.shutdownConsumidor()
  await consumidorTesteImpressao.shutdownConsumidor()
  await consumidorTesteInstanciamento.shutdownConsumidor()
  done()
})

describe('Testando filas de saida e seus conteúdos após envio de prova virtual', () => {
  test('A fila de impressão deve estar vazia', done => {
    jest.setTimeout(10000)

    const idReq1 = Math.floor(Math.random() * 10000) + 1

    enviarMensagemTesteVirtual(idReq1, function() {
      sleep(5000).then(() => {
        expect(consumidorTesteImpressao.dadosDaFila).toBeNull()
        done()
      })
    })
  })
})

describe('Testando filas de saida e seus conteúdos após envio de prova em papel', () => {
  test('A fila de impressão deve estar vazia', done => {
    jest.setTimeout(10000)

    const idReq1 = Math.floor(Math.random() * 10000) + 1

    enviarMensagemTestePapel(idReq1, function() {
      sleep(5000).then(() => {
        expect(consumidorTesteImpressao.dadosDaFila).toBeNull()
        expect(Array.isArray(consumidorTesteImpressao.dadosDaFila)).toBeFalsy()
        done()
      })
    })
  })
})

function sleep(time) {
  return new Promise(resolve => setTimeout(resolve, time))
}
