FROM node:8-slim

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY . /usr/src/app

EXPOSE 3100

RUN mkdir -p /usr/src/app
RUN chmod 777 -R /usr/src/app

CMD [ "npm", "start" ]
